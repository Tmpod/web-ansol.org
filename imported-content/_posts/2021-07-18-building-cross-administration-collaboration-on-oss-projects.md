---
categories: []
metadata:
  event_location:
  - event_location_value: Online
  event_site:
  - event_site_url: https://ec.europa.eu/eusurvey/runner/OSORwebinar5August2021
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-08-04 23:00:00.000000000 +01:00
    event_start_value2: 2021-08-04 23:00:00.000000000 +01:00
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 809
layout: evento
title: Building cross-administration collaboration on OSS projects
created: 1626614177
date: 2021-07-18
---

