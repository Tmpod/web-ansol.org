---
categories: []
metadata:
  event_location:
  - event_location_value: SETENTA & QUATRO Queluz Av. da República, nº99B 2745-208
      Queluz , Lisboa
  event_start:
  - event_start_value: 2019-07-24 19:00:00.000000000 +01:00
    event_start_value2: 2019-07-24 22:00:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 680
layout: evento
title: Encontro Ubuntu-pt @ Queluz
created: 1562850056
date: 2019-07-11
---
<p>Mais um evento da comunidade Ubuntu na cidade de Queluz.</p><p>&nbsp;</p><p>O evento da comunidade Ubuntu em Queluz sendo que o objectivo é aproximar mais a nossa comunidade e reforçar a nossa paixão pelo o open-source e o Ubuntu.</p><p>&nbsp;</p><p>Claro que não nos esquecemos do mais importante a cerveja e conversa da melhor.</p><p>&nbsp;</p><p>O local é em Queluz, no bar, Setenta e Quatro.</p><p>Localização:<br>https://osm.org/go/b5cp3hbvc?m=<br>geo:38.75543,-9.25531?z=19</p>
