---
categories: []
metadata:
  event_location:
  - event_location_value: " Cace Cultural do Porto"
  event_site:
  - event_site_url: http://supera.org.pt/jornadas2017/
    event_site_title: Jornadas Supera 2017
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2017-05-31 23:00:00.000000000 +01:00
    event_start_value2: 2017-06-02 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 499
layout: evento
title: JORNADAS SUPERA 2017
created: 1491345760
date: 2017-04-04
---
﻿﻿As Jornadas SUPERA decorrerão de 1 a 3 de Junho (quinta-feira, sexta-feira e sábado) de 2017 nas instalações do Cace Cultural do Porto (IEFP).
O objectivo das jornadas é a promoção de tecnologia e actividades de apoio aos deficientes físicos.
São constituídas por quatro atividades principais e o conjunto terá a duração de três dias. São as seguintes:

1 – Conferência Científica: destinada à partilha de trabalhos e projetos de Investigação,  Desenvolvimento e Inovação. A chamada para a submissão de artigos encontra-se aberta .
2 – Workshops: ações de caráter mais prático tendo em vista a atualização e aperfeiçoamento de conhecimentos profissionais. Serão preferencialmente destinados a todo tipo de profissionais com interesse e/ou atividade na área e estudantes do ensino superior. A chamada para propostas de Workshops encontra-se aberta.
3 – Expo-SUPERA: Exposição de Tecnologias de Apoio e Acessibilidade: destinada preferencialmente a empresas de produtos e serviços nesta área, empresas de TIC, apresentação de trabalhos/projetos académicos e formação na área.
4 – TOM: Porto: Hackatona de Desenvolvimento de Tecnologias de Apoio e Acessibilidade.
￼
A sessão de abertura deste evento contará com a presença da Senhora Secretária de Estado da Ciência, Tecnologia e Ensino Superior, Prof.ª Doutora Fernanda Rollo. As Jornadas SUPERA contam também com apoio institucional da Secretaria de Estado do MCTES.
