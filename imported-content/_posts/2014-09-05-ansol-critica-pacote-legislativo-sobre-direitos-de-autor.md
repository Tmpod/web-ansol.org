---
categories:
- ansol
- governo
- direito de autor
metadata:
  tags:
  - tags_tid: 33
  - tags_tid: 42
  - tags_tid: 43
  node_id: 216
layout: article
title: ANSOL critica  Pacote Legislativo sobre Direitos de Autor
created: 1409956962
date: 2014-09-05
---
<p>A ANSOL publicou uma análise crítica ao pacoe legislativo proposto pelo Governo sob alguns aspectos do direito de autor em Portugual. Está disponível no nosso site sobre direito de autor em <a href="https://c.ansol.org/node/33" title="Pacote Legislativo sobre Direitos de Autor">https://c.ansol.org/node/33</a></p>
