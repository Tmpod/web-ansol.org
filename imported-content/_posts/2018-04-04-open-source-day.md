---
categories:
- open source day
- software
- meeting
metadata:
  event_location:
  - event_location_value: Hotel Marriott, Varsóvia, Polónia
  event_site:
  - event_site_url: https://opensourceday.com/en/index.html
    event_site_title: Open Source Day
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-05-22 23:00:00.000000000 +01:00
    event_start_value2: 2018-05-22 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 241
  - tags_tid: 159
  - tags_tid: 129
  node_id: 566
layout: evento
title: Open Source Day
created: 1522845661
date: 2018-04-04
---
<p><strong>Open Source Day</strong> is an international conference gathering fans of open solutions from <a href="https://en.wikipedia.org/wiki/Central_and_Eastern_Europe" title="Central and Eastern Europe">Central and Eastern Europe</a>. Mission of the event is to introduce <a href="https://en.wikipedia.org/wiki/Open_source" title="Open source" class="mw-redirect">open source</a> solutions to Polish public and business institutions and popularize it as a secure, efficient, cost saving alternative to proprietary software.<sup id="cite_ref-1" class="reference"><a href="https://en.wikipedia.org/wiki/Open_Source_Day#cite_note-1">[1]</a></sup> The conference has taken place in <a href="https://en.wikipedia.org/wiki/Warsaw" title="Warsaw">Warsaw</a> since its beginning in 2007. Participants are mainly managers, developers, technical officers of public, banking, and insurance industries.</p><p>The conference has become a platform for exchanging experience, contacts and use cases of open source solutions in fields of: <a href="https://en.wikipedia.org/wiki/Virtualization" title="Virtualization">virtualization</a>, <a href="https://en.wikipedia.org/wiki/Cloud_computing" title="Cloud computing">cloud computing</a>, <a href="https://en.wikipedia.org/wiki/Database" title="Database">database</a>, <a href="https://en.wikipedia.org/wiki/Big_data" title="Big data">big data</a>, <a href="https://en.wikipedia.org/wiki/Information_security" title="Information security">Information security</a>.</p>
