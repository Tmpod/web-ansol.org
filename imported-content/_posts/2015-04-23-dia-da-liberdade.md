---
categories: []
metadata:
  event_location:
  - event_location_value: Avenida da Liberdade, Lisboa
  event_site:
  - event_site_url: https://www.nao-ao-ttip.pt/manifestacao-dia-25-de-abril-lisboa/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-04-24 23:00:00.000000000 +01:00
    event_start_value2: 2015-04-24 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 312
layout: evento
title: Dia da Liberdade
created: 1429828220
date: 2015-04-23
---
<p>Todos os anos a ANSOL vai à rua no 25 de Abril - Dia da Liberdade - para dar a conhecer a liberdade que vem do Software Livre.</p><p>Este ano, juntamo-nos à manifestação contra o TTIP e o CETA.</p>
