---
categories: []
metadata:
  event_location:
  - event_location_value: Reitoria da Universidade Nova de Lisboa
  event_site:
  - event_site_url: http://www.acessoaberto.pt/c/index.php/confoa2012/confoa2012
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2012-09-30 23:00:00.000000000 +01:00
    event_start_value2: 2012-10-01 23:00:00.000000000 +01:00
  node_id: 83
layout: evento
title: 3ª Conferência Luso-Brasileira sobre Acesso Aberto
created: 1344122755
date: 2012-08-05
---

