---
categories:
- sfd
- dia do software livre
- software freedom day
- drupal
- drupal-pt
- "#publiccode"
- debate
metadata:
  event_location:
  - event_location_value: Aveiro Expo – Parque de Exposições de Aveiro
  event_site:
  - event_site_url: http://techdays.pt/#program
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2017-10-13 23:00:00.000000000 +01:00
    event_start_value2: 2017-10-13 23:00:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 99
  - tags_tid: 197
  - tags_tid: 47
  - tags_tid: 64
  - tags_tid: 198
  - tags_tid: 196
  - tags_tid: 48
  node_id: 515
layout: evento
title: Software Freedom Day - TechDays Aveiro
created: 1501967122
date: 2017-08-05
---
<p>Em 2017, a ANSOL celebrou o Software Freedom Day no TechDays em Aveiro, juntamente com a comunidade Drupal PT.</p><p>&nbsp;<img src="https://groups.drupal.org/files/drupal_day_2017.jpg" width="640" height="360"></p><h3>Debate VIP: "Com dinheiro público, só Software Livre" - 10h - 12h30</h3><p>&nbsp; &nbsp; &nbsp; &nbsp;Imagine que o Estado vai comprar um carro <span class="il">de</span> uma marca muita boa e muito avançada. No entanto, em nenhuma circunstância pode abrir o capô do carro. Todos os consertos ou alterações só podem ser feitos na empresa que o vendeu e a sua condução obriga a uma formação especializada que só existe nessa mesma empresa. Assim o Estado, com o dinheiro dos contribuintes, vê-se obrigado a pagar não só pelo carro mas também por contratos <span class="il">de</span> manutenção que lhe ficam caros. Pode chamar a este carro <span class="il">de</span> "software" e o Estado gasta cerca <span class="il">de</span> 180 milhões <span class="il">de</span> euros anualmente nestas condições. Será isto um bom negócio para o Estado? Haverá alternativas?</p><p><br> &nbsp; &nbsp; &nbsp; &nbsp; A ANSOL - Associação Nacional para o Software livre vai promover um debate VIP intitulado "Com dinheiro público, só Software Livre". O evento ocorre no dia 14 <span class="il">de</span> Outubro <span class="il">de</span> 2017 entre as 10h e as 12h30m em Aveiro e está integrado no Techdays, o grande fórum <span class="il">de</span> tecnologia em Portugal (<a href="http://www.techdays.pt/" target="_blank" rel="noreferrer" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://www.techdays.pt/&amp;source=gmail&amp;ust=1507891962390000&amp;usg=AFQjCNFbSw0uJFBxZXt1OdmOXvpCodlksQ">http://www.techdays.pt/</a>).</p><p><br> &nbsp; &nbsp; &nbsp; &nbsp; Ao estilo do debate universitário, <span class="il">de</span> modo divertido e informativo, com a ajuda da Sociedade <span class="il">de</span> Debates da Universidade do Porto, convidamos a Engª Ana I. Branco (Agência para Modernização Administrativa), Prof. Carlos Costa (Lisbon School of Economics &amp; Management) e Engº Luís Sousa (ASSOFT - Associação Portuguesa <span class="il">de</span> Software) para esgrimir argumentos e responder às questões do público.</p><p><br> &nbsp; &nbsp; &nbsp; &nbsp; Não perca este grande debate! Registe-se em <a href="http://www.techdays.pt/" target="_blank" rel="noreferrer" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://www.techdays.pt/&amp;source=gmail&amp;ust=1507891962390000&amp;usg=AFQjCNFbSw0uJFBxZXt1OdmOXvpCodlksQ">http://www.techdays.pt/</a> e inscreva-se na sessão "CONFERÊNCIA: Software Livre e Tecnologia Drupal" para reservar o seu lugar no auditório!</p><p>Gravação audio (parcial) em https://ansol.org/recordings/techdays1.ogg .</p><h3>Drupal Day - 15h</h3><p>À tarde, no Auditório Principal, e em conjunto com a comunidade Portuguesa de Drupal, haverá uma série de apresentações relacionadas com Software Livre no geral e Drupal em particular:</p><p>15:00 - Software Livre em Portugal - Marcos Marado (ANSOL)</p><p>15:35 - Cultural Heritage &amp; Drupal - Alberto Permury</p><p>16:10 - Drupal 8: Internet of Things - Rui Figueiredo</p><p>16:45 - Encerramento</p><p>17:00 - Coffee Break e Networking</p><p>Gravação audio:</p><p><iframe src="https://archive.org/embed/DupalDayPT2017" width="500" height="140" frameborder="0"></iframe></p>
