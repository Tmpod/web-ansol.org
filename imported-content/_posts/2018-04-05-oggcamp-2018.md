---
categories:
- floss
- open tech
- meeting
metadata:
  event_location:
  - event_location_value: Sheffield, UK
  event_site:
  - event_site_url: http://oggcamp.org/
    event_site_title: Oggcamp
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-08-17 23:00:00.000000000 +01:00
    event_start_value2: 2018-08-18 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 273
  - tags_tid: 282
  - tags_tid: 129
  node_id: 595
layout: evento
title: OggCamp 2018
created: 1522934027
date: 2018-04-05
---
<p>OggCamp is an unconference celebrating Free Culture, Free and Open Source Software, hardware hacking, digital rights, and all manner of collaborative cultural activites. You can find out what happens at an OggCamp by watching the video below:</p><p>https://youtu.be/K15PIGuiLKw<br><br></p><p><br> <br><br><br></p>
