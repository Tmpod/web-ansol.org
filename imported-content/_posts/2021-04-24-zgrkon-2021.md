---
categories: []
metadata:
  event_location:
  - event_location_value: Online
  event_site:
  - event_site_url: https://ozgurkon.org/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-05-28 23:00:00.000000000 +01:00
    event_start_value2: 2021-05-29 23:00:00.000000000 +01:00
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 794
layout: evento
title: ÖzgürKon 2021
created: 1619298060
date: 2021-04-24
---
<p>ÖzgürKon is an international online conference, organized by Özgür Yazılım Derneği. COVID-19 is created an event vacuum since people are no longer able to gather physicaly. ÖzgürKon is aimed to fill this void by bringing worldwide free software community together in virtual space and underline the importance of free software in the light of current developments.</p>
