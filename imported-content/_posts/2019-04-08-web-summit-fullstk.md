---
categories: []
metadata:
  event_location:
  - event_location_value: Lisbon
  event_site:
  - event_site_url: https://websummit.com/fullstk
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-11-04 00:00:00.000000000 +00:00
    event_start_value2: 2019-11-07 00:00:00.000000000 +00:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 664
layout: evento
title: Web Summit - FullSTK
created: 1554720262
date: 2019-04-08
---
<p>Within the Web Summit, FullSTK,&nbsp;the world’s leading developer conference, showcases some of the world’s most impressive developers, engineers, investors and data scientists.</p><p>One of FullSTK's four tracks is dedicated to "Open Source".</p>
