---
categories: []
metadata:
  event_location:
  - event_location_value: IST, Lisboa
  event_site:
  - event_site_url: http://www.meetup.com/GeeksIn-Lisbon/events/233972259/?gj=co2&rv=co2
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-09-19 18:00:00.000000000 +01:00
    event_start_value2: 2016-09-19 18:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 453
layout: evento
title: Evento público W3C
created: 1474294513
date: 2016-09-19
---
<p>A W3C veio a Lisboa para um encontro a decorrer durante a semana.</p><p>Mas na segunda-feira, dia 19, terão também um evento público, para o qual a presença é aberta, sendo apenas necessário indicar a ida no site do evento.</p>
