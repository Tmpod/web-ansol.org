---
categories: []
metadata:
  event_location:
  - event_location_value: Online
  event_site:
  - event_site_url: https://meet.jit.si/Palestra_ANSOL
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-07-14 17:30:00.000000000 +01:00
    event_start_value2: 2021-07-14 17:30:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 806
layout: evento
title: Como contribuir para projectos Open Source
created: 1625930796
date: 2021-07-10
---
<p>A convite do <a href="https://glua.ua.pt/">GLUA - Grupo de Utilizadores de Linux da Universidade de Aveiro</a>, a ANSOL vai palestrar aos interessados - online - sobre "Como contribuir para projectos Open Source".</p><p><img src="https://ansol.org/sites/ansol.org/files/ansol.png" alt="poster do evento" style="display: block; margin-left: auto; margin-right: auto;" width="3000" height="3000"></p>
