---
categories:
- libreoffice
- formação
metadata:
  event_location:
  - event_location_value: Fundação Portuguesa das Comunicações, Lisboa
  event_site:
  - event_site_url: http://www.libreoffice.pt/2015/01/sessao-de-formacao-libreoffice-e-documentacao-digital-livre-na-educacao/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-02-21 00:00:00.000000000 +00:00
    event_start_value2: 2015-02-21 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 60
  - tags_tid: 61
  node_id: 275
layout: evento
title: 'Sessão de formação: LibreOffice e Documentação Digital Livre na educação'
created: 1422479211
date: 2015-01-28
---
<p>O Centro de Formação da Associação Nacional de Professores de Informática vai realizar uma sessão de formação de curta duração sobre LibreOffice e Documentação Digital Livre na educação, com a colaboração da Comunidade LibreOffice Portugal.</p><p>Objetivos:</p><p>Abordar as Normas Abertas, Interoperabilidade Digital e LibreOffice.</p><p>Esclarecer e difundir a utilização dos formatos de documentos abertos, inteirar sobre a interoperabilidade digital e atribuir competências iniciais aos formandos para trabalhar com a ferramenta de escritório livre, o LibreOffice e alguns dos principais módulos, o processador de texto Writer e a folha de cálculo Calc.</p><p>Responder à crescente necessidade e procura por parte de profissionais, professores e estudantes, de ferramentas de escritório de custo reduzido ou nulo, que possam responder às actuais necessidades de mercado e ao mesmo tempo respeitem as normas do mais recente standard europeu para documentos e modelos de documento aberto aprovado como ISO 26300.</p><p>Formador: Adriano Afonso | Comunidade LibreOffice Portugal</p><p>Local: Lisboa,&nbsp;<a href="http://www.fpc.pt/" target="_blank">Fundação Portuguesa das Comunicações</a></p><p>Data: 21 de Fevereiro, pelas 9.30h até às 13.00h</p><p>Confirmação&nbsp; das inscrições: 16 de fevereiro de 2015</p><p>Mais informações e inscrições&nbsp;<a href="http://goo.gl/forms/y9fh91mhlE" target="_blank">aqui</a>!</p><p>Pré-requisito:</p><p>A sessão será dinamizada com o mínimo de 10 inscrições<br> Critérios de seleção:</p><p>1º Ser sócio da ANPRI<br> 2º Pertencer ao grupo de Informática<br> 3º Outros participantes</p>
