---
categories:
- press release
- drm
- imprensa
metadata:
  tags:
  - tags_tid: 9
  - tags_tid: 10
  - tags_tid: 19
  node_id: 62
layout: article
title: 'PRESS RELEASE ANSOL: Dia Contra o DRM'
created: 1336119620
date: 2012-05-04
---
<p>Lisboa, 4 de Maio de 2012. A ANSOL - Associação Nacional para o Software Livre - celebra o facto de dois partidos políticos Portugueses com assento parlamentar, BE e PCP, levarem a debate no Parlamento dois Projetos de Resolução recomendando o Governo Português a desvincular-se do tratado ACTA - Acordo Comercial Anti-Contrafação, precisamente na mesma data do <a href="http://www.defectivebydesign.org/dayagainstdrm/">Dia Contra o DRM</a>.</p><p>Todos os anos, o dia 4 de Maio é celebrado como o Dia Contra o DRM. Digital Restriction Management são tecnologias digitais utilizadas para controlar e restringir aquilo que os utilizadores podem fazer com os objetos tecnológicos ou digitais que compram. Apesar de reconhecido pela indústria que estas tecnologias não são efetivas nem cumprem os seus propósitos, continuam a ser amplamente usadas e protegidas por legislação, impedindo os utilizadores de exercer os seus direitos e liberdades concedidos pela mesma. Por exemplo, ao colocar um mecanismo anti-cópia num DVD, o criador da obra está a impedir o comprador de poder exercer o seu direito à cópia privada.</p><p>"O ACTA piora esta situação, ameaçando diretamente o Software Livre ao tornar mais difícil sua a distribuição com partilha de ficheiros e tecnologias P2P como o BitTorrent, aceder a conteúdos pois o DRM não pode ser acedido legalmente com Software Livre, leitores portáteis de média que suportam formatos livres são menos comuns que os dispositivos que suportam DRM tornando-os suspeitos nas alfândegas e para os guardas fronteiriços" diz Marcos Marado, Vice-Presidente da ANSOL.</p><p>Rui Seabra, Presidente da ANSOL, acrescenta que "o ACTA cria uma cultura de vigilância e suspeita, na qual a liberdade que é necessária para produzir Software Livre é vista como perigosa e ameaçadora, em vez de criativa, inovadora e excitante."</p><p>A ANSOL considera que os Projetos de Resolução <a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=36799">n.º 232/XII/1.ª</a> (BE)[1] e&nbsp; <a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=36884">n.º 274/XII/1.ª</a> (PCP)[2] vêm oportunamente dar a hipótese à Assembleia da República de se manifestar frontalmente contra este tratado, mostrando ao Governo Português que o erro que cometeu pode e deve ser corrigido.</p>
