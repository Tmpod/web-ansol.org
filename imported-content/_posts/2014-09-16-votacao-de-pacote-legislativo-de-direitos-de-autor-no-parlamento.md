---
categories: []
metadata:
  event_location:
  - event_location_value: Assembleia da República
  event_site:
  - event_site_url: http://app.parlamento.pt/BI2/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2014-09-19 11:00:00.000000000 +01:00
    event_start_value2: 2014-09-19 11:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 224
layout: evento
title: Votação de Pacote Legislativo de Direitos de Autor no Parlamento
created: 1410892816
date: 2014-09-16
---
<p>Irão nesta data ser votados, pelo parlamento, as seguintes Propostas e Projectos de Lei:</p><p><a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=38642"><strong>Proposta de Lei n.º 245/XII/3.ª (GOV)</strong></a></p><p><a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=38643"><strong>Proposta de Lei n.º 246/XII/3.ª (GOV)</strong></a></p><p><a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=38644"><strong>Proposta de Lei n.º 247/XII/3.ª (GOV)</strong></a></p><p><a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=38647"><strong>Projeto de Lei n.º 646/XII/3.ª (PCP)</strong></a></p><p>&nbsp;</p>
