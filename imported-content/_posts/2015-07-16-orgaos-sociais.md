---
categories:
- ansol
- órgãos sociais
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 33
  - tags_tid: 100
  node_id: 334
layout: page
title: Órgãos Sociais
created: 1437071553
date: 2015-07-16
---
<h2>Órgãos Sociais</h2><h4><strong>Direcção</strong> <a href="mailto:direccao@ansol.org">direccao@ansol.org</a></h4><dl><dt><strong>Presidente</strong></dt><dd>Tiago Carrondo, <a href="mailto:tiago.carrondo@ansol.org">tiago.carrondo@ansol.org</a></dd><dt><strong>Vice-Presidente</strong></dt><dd>Marcos Marado, <a href="mailto:marcos.marado@ansol.org">marcos.marado@ansol.org</a></dd><dt><strong>Tesoureiro</strong></dt><dd>Octávio Gonçalves, <a href="mailto:octavio.goncalves@ansol.org">octavio.goncalves@ansol.org</a></dd><dt><strong>Vogal</strong></dt><dd>Daniel Sousa, <a href="mailto:daniel.sousa@ansol.org">daniel.sousa@ansol.org</a></dd><dt><strong>Secretário</strong></dt><dd>Rúben Mendes, <a href="mailto:ruben.mendes@ansol.org">ruben.mendes@ansol.org</a></dd></dl><h4><strong>Mesa da Assembleia</strong> <a href="mailto:mesa.assembleia@ansol.org">mesa.assembleia@ansol.org</a></h4><dl><dt><strong>Presidente</strong></dt><dd>Rui Seabra, <a href="mailto:rui.seabra@ansol.org">rui.seabra@ansol.org</a></dd><dt><strong>1º Secretário</strong></dt><dd>André Esteves, <a href="https://ansol.org/andre.esteves@ansol.org">andre.esteves@ansol.org</a></dd><dt><strong>2º Secretário</strong></dt><dd>Tiago Policarpo, <a href="mailto:tiago.policarpo@ansol.org">tiago.policarpo@ansol.org</a></dd></dl><h4><strong>Conselho Fiscal</strong> <a href="mailto:conselho.fiscal@ansol.org">conselho.fiscal@ansol.org</a></h4><dl><dt><strong>Presidente</strong></dt><dd>Jaime Pereira, <a href="mailto:jaime.pereira@ansol.org">jaime.pereira@ansol.org</a></dd><dt><strong>1º Secretário</strong></dt><dd>Sandra Fernandes, <a href="mailto:sandra.fernandes@ansol.org">sandra.fernandes@ansol.org</a></dd><dt><strong>2º Secretário</strong></dt><dd>Diogo Figueira, <a href="mailto:diogo.figueira@ansol.org">diogo.figueira@ansol.org</a></dd></dl><h4><strong>Contactar todos os Órgãos Sociais</strong></h4><p><a href="mailto:orgaos.sociais@ansol.org">orgaos.sociais@ansol.org</a></p>
