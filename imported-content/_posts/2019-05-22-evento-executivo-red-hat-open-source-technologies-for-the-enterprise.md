---
categories: []
metadata:
  event_location:
  - event_location_value: Porto
  event_site:
  - event_site_url: https://www.syone.com/events/executive-events-red-hat-open-source-technologies
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-05-27 23:00:00.000000000 +01:00
    event_start_value2: 2019-05-27 23:00:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 671
layout: evento
title: 'Evento Executivo Red Hat: Open Source Technologies for the Enterprise'
created: 1558557522
date: 2019-05-22
---

