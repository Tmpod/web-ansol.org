---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa
  event_site:
  - event_site_url: https://websummit.com/fullstk
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2017-11-06 00:00:00.000000000 +00:00
    event_start_value2: 2017-11-09 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 514
layout: evento
title: Web Summit - FullSTK
created: 1501966885
date: 2017-08-05
---
<p>Tal como no ano passado, na Web Summit de 2017, a decorrer em Lisboa, haverá a conferência "FullSTK", onde um dos seis temas será o "Open Source".</p>
