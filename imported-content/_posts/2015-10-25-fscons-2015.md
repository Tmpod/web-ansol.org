---
categories:
- free culture
- free software
- free society
- scandinavia
metadata:
  event_location:
  - event_location_value: Gothenburg, Sweden
  event_site:
  - event_site_url: https://fscons.org/2015/
    event_site_title: FSCon 2015
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-11-07 00:00:00.000000000 +00:00
    event_start_value2: 2015-11-08 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 110
  - tags_tid: 122
  - tags_tid: 123
  - tags_tid: 124
  node_id: 361
layout: evento
title: FSCONS 2015
created: 1445731572
date: 2015-10-25
---
<p>The Free Society Conference and Nordic Summit (FSCONS) is a meeting place for social change, focused on the intersection between technology, culture and society. FSCONS is organised according to the principles laid out in our<a href="https://fscons.org/2015/manifesto.html">Manifesto</a>, a living document accepted by the society’s members at the yearly meeting.</p><p>The conference brings together people from a wide range of fields, and merges the technical with the social, seeking both to activate and challenge. Open discussion and brainstorming are as important as the talks given during the conference.</p><p>&nbsp;</p>
