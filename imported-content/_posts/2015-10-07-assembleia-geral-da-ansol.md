---
categories:
- ag
- assembleia geral
- ansol
metadata:
  event_location:
  - event_location_value: Espaço 113, Porto
  event_start:
  - event_start_value: 2015-10-17 14:00:00.000000000 +01:00
    event_start_value2: 2015-10-17 14:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 101
  - tags_tid: 97
  - tags_tid: 33
  node_id: 348
layout: evento
title: Assembleia Geral da ANSOL
created: 1444248137
date: 2015-10-07
---
<p>Convoca-se todos os sócios da ANSOL para a Assembleia Geral que terá lugar no dia 17 de Outubro de 2015 pelas 15 horas no Espaço 113, Rua Nova do Seixo – Praceta da Concórdia, 113, 4465-092 PORTO, com a seguinte ordem de trabalhos:</p><p>- Apresentação, discussão e votação de propostas de alteração aos estatutos e regulamento interno da Associação;<br>- Outros assuntos.</p><p>Se à hora marcada não estiverem presentes, pelo menos, metade dos associados a Assembleia Geral reunirá, em 2ª convocatória, no mesmo local e passados 30 minutos, com qualquer número de presenças.<br><br></p><p>Localização:</p><p>Coordenadas GPS: 41.181527 , -8.630244<br>https://www.facebook.com/espaco113<br>O Espaço 113 situa-se na zona residencial do Monte dos Burgos, próximo de escolas do 1o ciclo ao secundário. O acesso pode ser feito através da Rua de Diu ou pela Rua da Cooperação. É servido por dois autocarros dos STCP: a linha 508 (Boavista - Cabo do Mundo) e a linha 602 (Cordoaria - Aeroporto).</p>
