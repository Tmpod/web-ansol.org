---
categories:
- sfd
metadata:
  event_location:
  - event_location_value: ISEP, Porto
  event_site:
  - event_site_url: https://sfd.portolinux.net/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-09-19 13:30:00.000000000 +01:00
    event_start_value2: 2015-09-19 13:30:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 99
  node_id: 328
layout: evento
title: Software Freedom Day 2015
created: 1433101042
date: 2015-05-31
---
<div style="float: right; padding: 1em;">&nbsp;</div><p>Um conjunto de elementos da comunidade Porto Linux, com o apoio da ANSOL, juntaram-se para organizar, na cidade do Porto, um evento comemorativo do Software Freedom Day, a 19 de Setembro de 2015.</p><p>A celebração do Porto Linux terá lugar no ISEP, a partir das 14h30, e será distribuída por três espaços:<br> &nbsp; - o primeiro é focado em desenvolvimento de aplicações Web e Mobile;<br> &nbsp; - o segundo está direccionado para Big Data, Internet of Things e gestão de contribuições no desenvolvimento de software;<br> &nbsp; - o terceiro espaço tem como nome de código "playground" e onde qualquer um pode fazer demonstrações, continuar conversas com oradores, etc.<br> <br> O programa completo pode ser visto no <a href="https://sfd.portolinux.net/">site do evento</a>.<br> <br> O acesso ao evento será feito pela Rua de S. Tomé.<br> <br> São todos muito bem-vindos!</p>
