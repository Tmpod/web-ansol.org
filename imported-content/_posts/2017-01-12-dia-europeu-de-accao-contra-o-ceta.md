---
excerpt: "O próximo dia 21 de Janeiro será o Dia Europeu de Acção pelo comércio justo
  e contra o CETA, e também em Portugal estão previstas actividades.\r\n\r\nPara Lisboa
  está já agendada uma concentração no Rossio pelas 14h. \r\nJunta, a sociedade civil
  irá declarar o Rossio como Zona Livre de CETA, TTIP e TISA, a nona em Portugal."
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa, Rossio
  event_site:
  - event_site_url: https://www.facebook.com/events/944884262308393/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2017-01-21 14:00:00.000000000 +00:00
    event_start_value2: 2017-01-21 14:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 476
layout: evento
title: Dia Europeu de Acção contra o CETA
created: 1484259322
date: 2017-01-12
---
<p>Aproxima-se o momento da votação no Parlamento Europeu do <a href="https://www.nao-ao-ttip.pt/compreender-o-ceta/">Acordo Económico Comercial Global, vulgo CETA</a>, entre o Canadá e a União Europeia, agendado para o próximo mês de Fevereiro.</p><p>Negociado desde 2009 entre dois dos maiores blocos económicos mundiais, o CETA visa ser um modelo para o comércio internacional.</p><p>Infelizmente, este tratado <a href="https://drm-pt.info/2016/10/16/o-ceta-e-o-drm/">não respeita as liberdades dos utilizadores de Software Livre</a>. Tendo já merecido a clara oposição de organismos das <a href="http://www.ohchr.org/EN/NewsEvents/Pages/DisplayNews.aspx?NewsID=20787">Nações Unidas</a>, <a href="http://www.smmp.pt/?p=27596">dos Juízes Europeus</a> , <a href="https://www.ttip-free-zones.eu/sites/default/files/materials/Barcelona%20declaration.pdf">Municípios</a> e de milhões de <a href="http://www.s2bnetwork.org/wp-content/uploads/2016/11/Transatlantic-CETA-statement.pdf" target="_blank" class="gmail_msg" data-saferedirecturl="https://www.google.com/url?q=http://www.s2bnetwork.org/wp-content/uploads/2016/11/Transatlantic-CETA-statement.pdf&amp;source=gmail&amp;ust=1484345080645000&amp;usg=AFQjCNHSys_rsJ3yJwBQlguSY6g-MoUZFg">associações e cidadãos de toda a UE e Canadá</a>, também a <a href="https://www.fsf.org/blogs/licensing/the-many-headed-monster-of-international-trade-agreements">FSF</a>, a <a href="https://blogs.fsfe.org/lucile.falg/2014/08/11/ttip-policy-laundering-a-few-reasons-for-free-software-communities-to-get-angry/">FSFE</a> e a <a href="https://blog.ffii.org/ceta-will-harm-our-privacy/">FFII</a> se têm mostrado contra este tratado.<br><br></p><p>Os protestos da sociedade civil tiveram já um claro impacto na negociação do CETA, bem como de outros acordos baseados nas mesmas premissas e este é o momento do processo de votação do CETA mais sensível à mobilização civil.</p><p>Assim, no seguimento do sucesso da concretização da discussão, no passado dia 12 de Janeiro, da petição nº124/XIII, que demanda um debate profundo sobre o CETA, urge continuar com a mobilização e informação.</p><p><strong>O próximo dia 21 de Janeiro será o <a href="http://stopceta.net/" target="_blank" class="gmail_msg" data-saferedirecturl="https://www.google.com/url?q=http://stopceta.net/&amp;source=gmail&amp;ust=1484345080645000&amp;usg=AFQjCNEejvhD5E3s-hR_B-NDh9JRj4pUTg">Dia Europeu de Acção pelo comércio justo e contra o CETA</a></strong><span class="gmail_msg">, e também em Portugal estão previstas actividades. </span></p><p>&nbsp;</p><pre class="m_-170515743871998674gmail-m_-7539061178639480785m_300182025619636933m_-6460784569695235468gmail-ace-line
gmail_msg" style="color: #000000; font-family: arial,sans-serif; padding-right: 1px; font-size: 13px;">&nbsp;</pre><pre class="m_-170515743871998674gmail-m_-7539061178639480785m_300182025619636933m_-6460784569695235468gmail-ace-line
gmail_msg" style="color: #000000; font-family: arial,sans-serif; padding-right: 1px; font-size: 13px;"><span class="m_-170515743871998674gmail-m_-7539061178639480785m_300182025619636933m_-6460784569695235468gmail-b
gmail_msg" style="padding-top: 1px; padding-bottom: 1px;"><span class="gmail_msg">Para Lisboa está já agendada uma concentração no Rossio pelas 14h. <br class="gmail_msg">Junta, a sociedade civil irá declarar o Rossio como Zona Livre de CETA, TTIP e TISA, a nona em Portugal.</span></span></pre><pre class="m_-170515743871998674gmail-m_-7539061178639480785m_300182025619636933m_-6460784569695235468gmail-ace-line
gmail_msg" style="color: #000000; font-family: arial,sans-serif; padding-right: 1px; font-size: 13px;">&nbsp;</pre><div class="m_-170515743871998674gmail-m_-7539061178639480785m_300182025619636933m_-6460784569695235468gmail-ace-line
gmail_msg" style="color: #000000; font-family: arial,sans-serif; padding-right: 1px; font-size: 13px; text-align: justify;"><p>Através de uma acção concertada conseguiremos rejeitar o CETA e o TTIP, tal como ocorreu no passado com a rejeição de iniciativas semelhantes como o <a href="https://en.wikipedia.org/wiki/Multilateral_Agreement_on_Investment" target="_blank" class="gmail_msg" data-saferedirecturl="https://www.google.com/url?q=https://en.wikipedia.org/wiki/Multilateral_Agreement_on_Investment&amp;source=gmail&amp;ust=1484345080645000&amp;usg=AFQjCNFqtvEbsmiOFMN10uMRaLEMrVKRGg">Acordo Multilateral de Investimento da OCDE</a> e o <a href="http://www.europarl.europa.eu/news/en/news-room/20120703IPR48247/european-parliament-rejects-acta" target="_blank" class="gmail_msg" data-saferedirecturl="https://www.google.com/url?q=http://www.europarl.europa.eu/news/en/news-room/20120703IPR48247/european-parliament-rejects-acta&amp;source=gmail&amp;ust=1484345080645000&amp;usg=AFQjCNFoZT1TVInF4dJKb3a5yMbymPoQoQ">Acordo Comercial de Combate à Contrafacção(ACTA)</a>, demonstrando neste percurso a possibilidade da obtenção de prosperidade através de modelos de comércio internacional mais justos.</p></div>
