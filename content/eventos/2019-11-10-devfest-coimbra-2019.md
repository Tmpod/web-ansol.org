---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 706
  event:
    location: Coimbra
    site:
      title: ''
      url: https://devfest.gdgcoimbra.xyz/
    date:
      start: 2019-11-16 00:00:00.000000000 +00:00
      finish: 2019-11-16 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: DevFest Coimbra 2019
created: 1573413784
date: 2019-11-10
aliases:
- "/evento/706/"
- "/node/706/"
---

