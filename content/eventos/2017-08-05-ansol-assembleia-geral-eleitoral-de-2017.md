---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 516
  event:
    location: Fórmul@, São Mamede de Infesta
    site:
      title: 
      url: 
    date:
      start: 2017-08-15 14:00:00.000000000 +01:00
      finish: 2017-08-15 14:00:00.000000000 +01:00
    map: {}
layout: evento
title: ANSOL - Assembleia Geral Eleitoral de 2017
created: 1501967385
date: 2017-08-05
aliases:
- "/evento/516/"
- "/node/516/"
---
<p>A Assembleia Geral Eleitoral de 2017 terá lugar na Formul@ no Porto, no dia 15 de Agosto, com início às 14h00 da tarde, com a seguinte ordem de trabalhos:</p><p>- Debate do presente e futuro da ANSOL<br>- Organização de listas para os corpos sociais (nenhuma lista foi apresentada à mesa)<br>- Votação para os corpos sociais<br>- Outros Assuntos</p><p><br>Se à hora marcada não estiverem presentes, pelo menos, metade dos associados a Assembleia Geral reunirá, em 2a convocatória, no mesmo local e passados 30 minutos, com qualquer número de presenças.</p><p>Fórmul@,<br>Rua Bernardim Ribeiro 166 A,<br>4465-043 S. Mamede de Infesta</p>
