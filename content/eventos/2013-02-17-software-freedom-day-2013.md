---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 130
  event:
    location: 
    site:
      title: ''
      url: http://wiki.softwarefreedomday.org/2013/Portugal/Lisboa/ANSOL
    date:
      start: 2013-09-28 19:30:00.000000000 +01:00
      finish: 2013-09-28 19:30:00.000000000 +01:00
    map: {}
layout: evento
title: Software Freedom Day 2013
created: 1361066431
date: 2013-02-17
aliases:
- "/evento/130/"
- "/node/130/"
---
<p>Celebremos o Dia do Software Livre e o 30º aniversário GNU!</p><p>Dia 28 de Setembro, pelas 19h30.&nbsp; Localização: Restaurante A Muralha, em Alfama, Lisboa.</p><p>https://maps.google.com/maps/ms?msa=0&amp;ie=UTF8&amp;ll=38.711511,-9.127218&amp;spn=0,0&amp;msid=217341118419009317574.0004a7808ab3561ca7f0b&amp;source=embed</p>
