---
categories:
- open source
- free software
- linux
- espanha
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 127
  - tags_tid: 122
  - tags_tid: 242
  - tags_tid: 269
  node_id: 583
  event:
    location: La Térmica, Málaga, Espanha
    site:
      title: Open South Code2018
      url: https://www.opensouthcode.org/conferences/opensouthcode2018
    date:
      start: 2018-06-01 00:00:00.000000000 +01:00
      finish: 2018-06-02 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Opensouthcode 2018
created: 1522867217
date: 2018-04-04
aliases:
- "/evento/583/"
- "/node/583/"
---
<h3 class="text-center"><strong>Opensouthcode</strong> es un evento para promocionar y dar a conocer las tecnologías abiertas: software/hardware libre y opensource. El evento es gratuito.</h3>
