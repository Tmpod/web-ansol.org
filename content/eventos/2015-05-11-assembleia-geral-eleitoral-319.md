---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 319
  event:
    location: Casa Das Associações, Porto
    site:
      title: 
      url: 
    date:
      start: 2015-07-11 14:30:00.000000000 +01:00
      finish: 2015-07-11 14:30:00.000000000 +01:00
    map: {}
layout: evento
title: Assembleia Geral Eleitoral
created: 1431370066
date: 2015-05-11
aliases:
- "/evento/319/"
- "/node/319/"
---
<p><span style="font-size: 15px; font-family: Arial; color: #000000; background-color: transparent; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline;">Mais informação em https://ansol.org/node/318</span></p>
