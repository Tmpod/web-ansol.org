---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 335
  event:
    location: Lispolis, Lisboa
    site:
      title: ''
      url: http://www.eventolinux.org/
    date:
      start: 2015-10-01 00:00:00.000000000 +01:00
      finish: 2015-10-01 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Evento Linux 2015
created: 1437148095
date: 2015-07-17
aliases:
- "/evento/335/"
- "/node/335/"
---

