---
layout: evento
title: 1º Encontro Ubuntu-pt em Aveiro
metadata:
  event:
    location: Departamento de Eletrónica, Telecomunicações e Informática, 4, Avenida João Jacinto Magalhães, Glória, Glória e Vera Cruz, Aveiro, 3810-193, Portugal 
    site:
      url: https://loco.ubuntu.com/events/ubuntu-pt/4308-encontro-ubuntu-pt-aveiro/
    date:
      start: 2023-01-19
      finish: 2023-01-19
---

[![Cartaz](https://ansol.org/eventos/2023-01-19-ubuntu-pt-aveiro-1/cover.png )](https://www.openstreetmap.org/way/145482891#map=19/40.63316/-8.65939?layers=N)

Pela primeira vez a comunidade Ubuntu Portugal reúne-se em Aveiro.

Seremos acolhidos pelos nossos amigos do [Grupo de Linux da Universidade de Aveiro](https://glua.ua.pt), que nos vão dar a conhecer melhor as suas actividades e a Universidade.

Palestra na Universidade de Aveiro às 19h, seguido de encontro social na Adega do Evaristo às 20h.

Vem, traz um amigo ou um familiar e vem conviver e partilhar experiências com o resto da comunidade portuguesa...

Para facilitar as marcações na palestra e jantar pedimos que respondas a [este questionário](https://bitpoll.de/poll/ubpt-190123/vote/).

Se tiveres dúvidas contacta a organização [neste grupo de telegram](https://t.me/ubuntuptgeral), ou [no matrix](https://matrix.to/#/#ubuntu-pt:matrix.org).

### Locais:
 * Universidade de Aveiro: [Department of Electronics, Telecommunications and Informatics](https://www.openstreetmap.org/way/145482891#map=19/40.63316/-8.65939), 4, Avenida João Jacinto Magalhães, Glória, Glória and Vera Cruz, Aveiro
 * Adega do Evaristo: Rua 31 Janeiro, 14 3810-192, Aveiro
