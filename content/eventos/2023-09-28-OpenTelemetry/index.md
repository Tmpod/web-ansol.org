---
layout: evento
title: OpenTelemetry and Continuous Feedback
metadata:
  event:
    location: Praxis, Coimbra
    site:
      url: https://www.meetup.com/Coimbra-JUG/events/295614278
    date:
      start: 2023-09-28 18:30:00.000000000 +01:00
      finish: 2023-09-28 20:00:00.000000000 +01:00
---


Evento a decorrer em Inglês.

**OpenTelemetry and Continuous Feedback - Know about your code at runtime**

There are many tools and libraries that deploy Java code out into production, but how can we bring data and information back into our code?

Continuous Feedback is a new dev practice that provides developers with exceedingly fast feedback loops and code-change analysis to help navigate complex codebases and gain more confidence in your PRs.

Using OpenTelemetry developers can leverage specific metrics, logs, and traces to get immediate feedback as they develop, spot code smells and common issues, and later follow their code at scale as it gets released into the wild.
