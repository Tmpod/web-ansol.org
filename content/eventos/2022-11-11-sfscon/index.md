---
layout: evento
title: SFSCON - A Conference all about Free Software
metadata:
  event:
    date:
      start: 2022-11-11 08:30:00.000000000 +01:00
      finish: 2022-11-12 13:00:00.000000000 +01:00
    location: NOI Techpark - Bolzano, Itália
    site:
      url: https://www.sfscon.it/
--- 

![](cartaz.png)
