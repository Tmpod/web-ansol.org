---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 604
  event:
    location: Leipzig, Alemanha
    site:
      title: ''
      url: https://events.ccc.de/congress/2018/wiki/index.php
    date:
      start: 2018-12-27 00:00:00.000000000 +00:00
      finish: 2018-12-30 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: 35th Chaos Communication Conference
created: 1522942090
date: 2018-04-05
aliases:
- "/evento/604/"
- "/node/604/"
---

