---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 333
  event:
    location: Bruxelas
    site:
      title: ''
      url: http://www.politico.eu/article/european-parliament-green-lights-key-ttip-compromise-trade-deal/
    date:
      start: 2015-07-13 00:00:00.000000000 +01:00
      finish: 2015-07-17 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Ronda de negociações sobre o TTIP
created: 1436392658
date: 2015-07-08
aliases:
- "/evento/333/"
- "/node/333/"
---

