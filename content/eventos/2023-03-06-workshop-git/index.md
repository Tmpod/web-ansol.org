---
layout: evento
title: Workshop de Introdução ao git
metadata:
  event:
    location: Universidade de Aveiro
    site:
      url: https://workshop-git.aettua.pt
    date:
      start: 2023-03-06
      finish: 2023-03-06
---

A [AETTUA](https://aettua.pt) em conjunto com o [GLUA](https://glua.ua.pt/) organizou um workshop de Introdução ao [git](https://git-scm.com/) onde terás a oportunidade de ficar a conhecer esta plataforma e pôr alguns conceitos em prática.
