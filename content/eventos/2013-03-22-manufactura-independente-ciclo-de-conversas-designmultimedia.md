---
categories: []
metadata:
  node_id: 137
  event:
    location: Museu da Ciência, Coimbra
    site:
      title: ''
      url: https://www.facebook.com/events/542311742479529/
    date:
      start: 2013-04-02 17:00:00.000000000 +01:00
      finish: 2013-04-02 17:00:00.000000000 +01:00
    map: {}
layout: evento
title: MANUFACTURA INDEPENDENTE - Ciclo de Conversas Design+Multimédia
created: 1363953888
date: 2013-03-22
aliases:
- "/evento/137/"
- "/node/137/"
---
<p>Neste &quot;Ciclo de Conversas&quot;, desta vez apresenta-se o est&uacute;dio &quot;Manufactura Independente&quot;.</p>
<p><img src="https://fbcdn-sphotos-d-a.akamaihd.net/hphotos-ak-ash4/480719_450550775025443_193054432_n.jpg" style="width: 600px; height: 239px; " /></p>
<p>Manufactura Independente (Ana Isabel Carvalho e Ricardo Lafuente) &eacute; um est&uacute;dio de investiga&ccedil;&atilde;o &agrave; volta do design e metodologias livres, baseado no Porto. Trabalham &agrave; volta do software livre e cultura hacker, integrando essas &aacute;reas em v&aacute;rios projectos de design e tecnologias web, como a revista Libre Graphics (que co-editam), workshops colaborativas de design e tipografia ou o site independente de informa&ccedil;&atilde;o parlamentar Demo.cratica. Desde 2008 que tem dinamizado workshops e palestras sobre temas como design de interfaces, hacking pela cidadania ou empreendedorismo alternativo em Montr&eacute;al, Vars&oacute;via, Bergen, Roterd&atilde;o e Madrid, entre outras.&nbsp;</p>
