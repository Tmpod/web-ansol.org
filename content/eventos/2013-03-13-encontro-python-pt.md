---
categories: []
metadata:
  node_id: 134
  event:
    location: Multiversus, Rua do Bonjardim 618 Loja C, Porto
    site:
      title: ''
      url: https://plus.google.com/events/cps7aef27ikh9m6j97vc9cvob68
    date:
      start: 2013-04-06 00:00:00.000000000 +01:00
      finish: 2013-04-06 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Encontro Python.pt
created: 1363217293
date: 2013-03-13
aliases:
- "/evento/134/"
- "/node/134/"
---
<p>Programa:</p>
<ul>
	<li>
		11:00 - Talk: Usar Sentry (http://getsentry.com) para gest&atilde;o de exce&ccedil;&otilde;es em projetos (Jos&eacute; Moreira /@zemanel) </li>
	<li>
		11:30 - Talk: Web2py (Francisco Costa /@franciscosta) </li>
	<li>
		12:00 - Talk: a definir </li>
	<li>
		12:30 - Talk: a definir </li>
	<li>
		13:00 - almo&ccedil;o</li>
</ul>
