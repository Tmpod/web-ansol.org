---
layout: evento
title: Boas prácticas para a colaboração em projectos de Software Livre
metadata:
  event:
    date:
      start: 2023-10-17 21:30:00
      finish: 2023-10-17 22:30:00
    location: Online
    site:
      url: https://hacktoberfest.ansol.org
---

[![](cartaz.png)](https://hacktoberfest.ansol.org)

O Hacktoberfest é um evento que dura o mês inteiro de outubro onde as pessoas
são incentivadas a participar em projectos Software Livre / Open Source.

A ANSOL organiza uma sessão sobre boas prácticas para a colaboração em
projectos de Software Livre pela Joana Simões:

> O objetivo desta sessão é apresentar algumas boas práticas comportamentais para
> a colaboração com projetos de Software Livre, nomeadamente quando e como
> apresentar um PR e alguns aspectos da comunicação com maintainers e outros
> contribuidores. Como exemplo, vou apresentar o projeto pygeoapi, que se uniu
> este ano ao Hackfest, e que está a receber colaborações de novos
> contribuidores.

A Joana é uma entusiasta das tecnologias livres geospaciais, o que a levou a
tornar-se charter member da Open Geospatial Foundation (OSGeo) em 2017. Ao
longo dos anos, tem colaborado com vários projectos de software livre, em
particular com a pygeoapi, uma API de python para partilhar dados geospaciais.
Durante o dia, a Joana trabalha para o Open Geospatial Consortium (OGC), uma
organização que se dedica aos standards de informação geospacial e na sua
própria empresa, a ByteRoad.

Não é preciso inscrição prévia. Para participar, basta entrar à hora indicada
na página:

[hacktoberfest.ansol.org](https://hacktoberfest.ansol.org)
