---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 242
  event:
    location: 
    site:
      title: ''
      url: http://www.spautores.pt/destaques/ii-seminario-internacional-cultura-direito-de-autor-lusofonia-e-o-futuro
    date:
      start: 2014-11-03 00:00:00.000000000 +00:00
      finish: 2014-11-04 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: II Seminário Internacional | Cultura, Direito de Autor, Lusofonia e o Futuro
created: 1413566375
date: 2014-10-17
aliases:
- "/evento/242/"
- "/node/242/"
---

