---
categories: []
metadata:
  mapa:
  - mapa_geom: !binary |-
      AQEAAAAAAAAArk4iwGDg39HIX0NA
    mapa_geo_type: point
    mapa_lat: !ruby/object:BigDecimal 27:0.38748316034617e2
    mapa_lon: !ruby/object:BigDecimal 27:-0.9153671264648e1
    mapa_left: !ruby/object:BigDecimal 27:-0.9153671264648e1
    mapa_top: !ruby/object:BigDecimal 27:0.38748316034617e2
    mapa_right: !ruby/object:BigDecimal 27:-0.9153671264648e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.38748316034617e2
    mapa_geohash: eyckrtymrcxuxcpc
  slide:
  - slide_value: 0
  node_id: 653
  event:
    location: CLUBE ISCTE Edif. do ISCTE – ALA AUTÓNOMA Piso 4, Lisboa
    site:
      title: Ubucon Portugal 2019
      url: https://ubuconpt2019.ubuntu-pt.org/
    date:
      start: 2019-04-06 10:00:00.000000000 +01:00
      finish: 2019-04-06 18:00:00.000000000 +01:00
    map: {}
layout: evento
title: Ubucon Portugal 2019
created: 1552038153
date: 2019-03-08
aliases:
- "/UbuconPortugal2019/"
- "/evento/653/"
- "/node/653/"
---
<p><img src="https://ubuconpt2019.ubuntu-pt.org/assets/img/cartaz.jpg" alt="Poster" style="display: block; margin-left: auto; margin-right: auto;" width="922" height="1280"></p><p>A Comunidade Ubuntu Portugal, o ISCTE - Instituto Universitário de Lisboa, o ISTAR-IUL - Information Sciences and Technologies and Architecture Research Center e o ISCTE-IUL ACM Student Chapter, organizam no próximo dia 6 de Abril o evento “Ubucon Portugal 2019”.</p><p>Um encontro que tem por objectivo a divulgação do Sistema Operativo Ubuntu nas suas mais variadas aplicações. É aberto ao público em geral e dirigido a todos os interessados independentemente dos seus conhecimentos ou possíveis aplicações.</p><div class="footerTitle">A ANSOL estará presente com uma apresentação sobre a associação.</div><div class="footerTitle">&nbsp;</div><div class="footerTitle">ISCTE - INSTITUTO UNIVERSITÁRIO DE LISBOA</div><p>Av.ª das Forças Armadas, 1649-026 Lisboa<br><br>Geo URI: <a href="geo:38.7491,-9.1578?z=15" id="geo_uri">geo:38.7491,-9.1578?z=15<br></a>Localização: https://osm.org/go/b5cr8KP9--</p>
