---
categories: []
metadata:
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 808
  event:
    location: Online
    site:
      title: ''
      url: https://events.gnome.org/event/9/overview
    date:
      start: 2021-07-21 00:00:00.000000000 +01:00
      finish: 2021-07-25 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: GUADEC 2021 - A conferência GNOME
created: 1626024599
date: 2021-07-11
aliases:
- "/evento/808/"
- "/node/808/"
---

