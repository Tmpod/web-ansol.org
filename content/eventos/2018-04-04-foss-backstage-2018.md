---
categories:
- comunidade
- governance
- gestão
- floss
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 251
  - tags_tid: 272
  - tags_tid: 229
  - tags_tid: 273
  node_id: 585
  event:
    location: Kulturbrauerei, Berlim, Alemanha
    site:
      title: FOSS BackStage 2018
      url: https://foss-backstage.de/
    date:
      start: 2018-06-13 00:00:00.000000000 +01:00
      finish: 2018-06-14 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Foss Backstage 2018
created: 1522867949
date: 2018-04-04
aliases:
- "/evento/585/"
- "/node/585/"
---
<p>FOSS Backstage is a conference on <strong>governance</strong>, <strong>economics</strong>, <strong>legal</strong> issues, and <strong>collaboration</strong> within FOSS projects.<br> It’s not just another developer conference. FOSS Backstage provides a platform to discuss and learn about all non-technical aspects of open source development.</p><p>The founders of FOSS Backstage are <a href="http://isabel-drost.de/">Isabel&nbsp;Drost-Fromm</a> and <a href="http://www.stefan-rudnitzki.de/">Stefan Rudnitzki</a> in cooperation with <a href="http://newthinking.de/">Newthinking</a>. Isabel&nbsp;is the Open Source Strategist at Europace AG, a former Director of the Apache Software Foundation and PMC member of Apache Mahout. She says about FOSS Backstage:</p><blockquote><p>“Decentralised organisation, shared responsibility for leadership topics, but also corporate shenanigans are what every successful open source project has to deal with sooner or later. That’s why I am excited to be able to invite key free and open source players to Berlin/ Germany to discuss war stories and wisdom on running these projects otherwise hidden in plain sight. I'm looking forward to an event that helps connect those leading the free and open source movement.“</p></blockquote><p>Stefan is Lead Developer at Europace AG and co-organizer of Async Cat Herding meetup Berlin. The meetup deals with the challenges that come with collaborating across different cities, time zones and even continents. Stefan describes his motivation to start #FOSSBack like this:&nbsp;</p><blockquote><p>“If you are working as a developer, running an IT business or just using modern tools. There is a very high chance that the tools you use are somewhat connected to OpenSource. From my personal experience you will encounter many non-technical aspects while working for and with OpenSource. Things like decision making and collaboration will be part of these aspects as well as governance, finance and legal. I love the idea of helping to create something that focuses on these aspects and that supports people to grow, learn, share their experience and master these non-technical aspects.“</p></blockquote><p>at FOSS Backstage you can:</p><ul><li><strong>learn</strong> about<ul><li>leading a community of <em>volunteers</em></li><li><em>legal</em> aspects of FOSS such as licence compliance</li><li>open source <em>business models</em></li></ul></li><li><strong>connect</strong> with<ul><li>the <em>organisations</em> behind FOSS projects</li><li><em>companies</em> working with open source</li><li><em>people contributing</em> to open source projects</li></ul></li><li><strong>inspire</strong><ul><li>others to adopt your <em>best practices</em></li><li>yourself by visiting <em>Berlin</em>, a true hub of the free and open source software community</li><li>your <em>company or community</em> with the new approaches you learned at #FOSSBack</li></ul></li></ul><p>&nbsp;</p>
