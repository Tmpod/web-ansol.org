---
categories: []
metadata:
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 815
  event:
    location: Online
    site:
      title: ''
      url: https://www.wikidata.org/wiki/Wikidata:Events/Data_Quality_Days_2021
    date:
      start: 2021-09-08 00:00:00.000000000 +01:00
      finish: 2021-09-15 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'Wikidata: Data Quality Days 2021'
created: 1630926269
date: 2021-09-06
aliases:
- "/evento/815/"
- "/node/815/"
---

