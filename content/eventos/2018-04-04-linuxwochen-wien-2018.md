---
categories:
- free software
- open source
- meeting
- áustria
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 122
  - tags_tid: 127
  - tags_tid: 129
  - tags_tid: 250
  node_id: 576
  event:
    location: FH Technikum Wien, Viena, Austria
    site:
      title: Linuxwochen Wien 2018
      url: https://www.linuxwochen.at/index.php
    date:
      start: 2018-05-03 00:00:00.000000000 +01:00
      finish: 2018-05-05 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Linuxwochen Wien 2018
created: 1522857918
date: 2018-04-04
aliases:
- "/evento/576/"
- "/node/576/"
---
<p>Hier lassen sich Vorträge/Workshops/… einreichen: <a href="https://cfp.linuxwochen.at/de/LWW18/cfp/events/new">https://cfp.linuxwochen.at/de/LWW18/cfp/events/new</a></p><p>Sei es Open Source, Open Hardware, Creative Commons, oder Erfahrungen und Tipps für den Alltag – wir laden Dich ein Deine Projekte, Ideen und Erkenntnisse bei den Linuxwochen Wien 2018 anderen vorzustellen.</p><p>Vorträge für Ein- und Umsteiger, die noch wenig Erfahrung mit Open Source haben sind genauso gefragt, wie persönliche Strategien fortgeschrittener Anwender, die ihre Erfahrungen mit anderen Profis austauschen und diskutieren wollen.</p><p>Einblick über unsere Vielfalt vermittelt Dir auch unser Programm des Vorjahres: <a href="https://cfp.linuxwochen.at/de/lww17/public/schedule">https://cfp.linuxwochen.at/de/lww17/public/schedule</a></p><p>Bitte trage Deinen Vortrag/Workshop gleich in unserem CfP-Manager:<br> <a href="https://cfp.linuxwochen.at/de/LWW18/cfp/events/new">https://cfp.linuxwochen.at/de/LWW18/cfp/events/new</a></p><p>bis zum 19. März 2018 ein.</p><p>Ein Event wie die Linuxwochen Wien kann natürlich nicht ohne freiwillige Helfern funktionieren. Engagierte, helfende Hände, die im Vorfeld und/oder vor Ort (Betreuung Vortragende, Community Projekte, …) unterstützen wollen, sind herzlich willkommen. Bei Interesse melde Dich gleich bei <a href="mailto:programm2018@linuxwochen.at">programm2018@linuxwochen.at</a>.</p>
