---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 670
  event:
    location: Carcavelos
    site:
      title: ''
      url: http://libraries.fe.unl.pt/index.php/wikidata-days-2019-share-collaborate-transform-shaping-data-in-a-changing-world
    date:
      start: 2019-06-07 00:00:00.000000000 +01:00
      finish: 2019-06-08 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: WikiData Days 2019
created: 1558551110
date: 2019-05-22
aliases:
- "/evento/670/"
- "/node/670/"
---

