---
categories:
- evento
- apresentação
- software livre
- talk
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 84
  - tags_tid: 171
  - tags_tid: 41
  - tags_tid: 172
  node_id: 430
  event:
    location: EKA Palace, Calçada Dom Gastão, 12, Lisboa
    site:
      title: ''
      url: https://www.facebook.com/MoonDanceParty
    date:
      start: 2016-07-30 19:00:00.000000000 +01:00
      finish: 2016-07-30 19:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'Moon Dance: Introdução às Tecnologias Livres'
created: 1466713126
date: 2016-06-23
aliases:
- "/MoonParty/"
- "/evento/430/"
- "/node/430/"
---
<p><img src="https://scontent-mad1-1.xx.fbcdn.net/v/t1.0-9/13592731_1009320539187439_8932082172318474605_n.png?oh=036efaa44a6168cf4ccc8b52e51954bc&amp;oe=583392C5" alt="Flyer" style="display: block; margin-left: auto; margin-right: auto;" height="481" width="750"></p><p>A ANSOL alia-se à organização da festa musical "Moon Dance", que não se cinge a ser uma festa musical, tendo também um conjunto de palestras e workshops sobre temas relacionados com permacultura urbana, artes eletronico-digitais e tecnologias livres. Enquadrado neste evento, Marcos Marado, presidente da ANSOL, irá fazer uma apresentação entitulada "Introdução às Tecnologias Livres", aonde irá falar, entre outras coisas, sobre o que é o Software Livre, a sua importância, e dar alguns exemplos práticos.</p><p>As talks e workshops deste evento, de entrada gratuita, começam às 15:30, sendo a apresentação da ANSOL às 19:00.</p>
