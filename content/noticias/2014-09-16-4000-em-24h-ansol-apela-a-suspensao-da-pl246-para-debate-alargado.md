---
excerpt: A ANSOL (Associação Nacional para o Software Livre) opõe-se à proposta de
  lei 246/XII e deslocar-se-á às 12:00 à porta principal da Assembleia da República
  para entrega da petição.
categories:
- "#pl246"
- cópia privada
metadata:
  tags:
  - tags_tid: 45
  - tags_tid: 11
  node_id: 222
layout: article
title: '4000 em 24h! ANSOL apela à suspensão da #pl246 para debate alargado'
created: 1410888480
date: 2014-09-16
aliases:
- "/article/222/"
- "/node/222/"
- "/pr-20140916-2/"
---
<p>Lisboa, 16 de Setembro de 2014 - A ANSOL (Associação Nacional para o Software Livre) opõe-se à proposta de lei 246/XII e deslocar-se-á às 12:00 à porta principal da Assembleia da República para entrega da petição.</p><p>«<em>O Parlamento não pode ignorar esta petição, que reuniu mais de 4000 assinaturas em cerca de 24h</em>», diz Rui Seabra, presidente da Direção da ANSOL referindo-se ao regulamento da Assembleia da República obriga a que as petições com mais de 4000 subscritores sejam debatidas em Plenário.</p><p>Acrescenta ainda que «<em>é imperativo suspender o processo de discussão da PL 246 e dar oportunidade a uma discussão pública e aberta sobre conteúdos digitais e a&nbsp; reforma do direito de autor</em>».</p><p>Às 18:20 a petição conta já com quase 4300 assinaturas e encontra-se disponível em <a href="http://bit.do/pl246">http://bit.do/pl246</a></p>
