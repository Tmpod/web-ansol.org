---
categories:
- press release
- acta
- manifestação
- imprensa
metadata:
  tags:
  - tags_tid: 9
  - tags_tid: 12
  - tags_tid: 13
  - tags_tid: 19
  node_id: 79
layout: article
title: ANSOL e Tugaleaks protestam contra ACTA
created: 1338851506
date: 2012-06-05
aliases:
- "/article/79/"
- "/node/79/"
- "/pr-20120605/"
---
<p>Lisboa, 5 de Junho de 2012: A <a href="https://ansol.org/">Associação Nacional para o Software Livre</a> e o movimento <a href="http://www.tugaleaks.com">Tugaleaks</a> organizam uma <a href="https://ansol.org/acta/protesto-20120609">manifestação de protesto em Lisboa e Coimbra</a> contra o <a href="https://ansol.org/acta">ACTA</a>, um acordo trilateral supostamente anti contrafação mas negociado em segredo e com ramificações profundas contra a liberdade de expressão e o livre acesso à medicina e informática da parte dos cidadãos.</p><p><em>A opacidade política não podia ser mais extravagante: enquanto que parlamentos e congressos não tinham acesso ao texto do ACTA, as grandes editoras de conteúdos, farmacêuticas, e outros poderosos grupos liam e sugeriam novos conteúdos</em>», afirma Rui Seabra, presidente da Direção da ANSOL.</p><p>As versões iniciais do ACTA foram apenas conhecidas através de fugas de informação que levaram à publicação online dos documentos, sendo que apenas a partir de um dos últimos rascunhos foi permitido sequer o Parlamento Europeu, com regras de transparência muito avançadas.</p><p>Muitos eurodeputados não aprovam o ACTA, efetivamente nas últimas semanas quatro dos cinco comités parlamentares que têm de analisar este tratado, DEVE, ITRE, JURI e LIBE já recomendaram claramente a rejeição do acordo numa votação a decorrer ainda durante o mês de Junho, e será provável que o quinto, o INTA, siga alinhado.</p><p>«<em>Ainda assim, <a href="http://www.computerworld.com.pt/2012/01/26/portugal-comprometido-com-assinatura-do-acta/">Portugal subscreveu o ACTA</a> sem qualquer transparência no início deste ano, sugerindo um desejo por medidas que restrinjam a liberdade de expressão e o acesso a medicamentos genéricos e a software livre</em>» acrescenta Rui Seabra.</p><p>Foram apresentadas duas recomendações ao governo para se desvincular do ACTA, uma <a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=36884">pelo Partido Comunista Português</a> e outra <a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=36799">pelo Bloco de Esquerda</a>, mas o PSD e CDS votaram contra, impedindo a sua viabilidade com o apoio implícito da abstenção do PS, fora uma deputada que votou a favor das recomendações.</p><p>«<em>Como movimento ativista online, tínhamos que nos opor a algo que vai inevitavemente corromper a Internet no seu ser, e na sua forma de liberdade a que estamos habituados. Apelamos portanto à consciencialização nacional para um protesto por uma Internet que seja mais livre e irrevogável.</em>», conclui Rui Cruz, do Tugaleaks.</p><h3>CONTACTOS</h3><p>ANSOL <a href="mailto:contacto@ansol.org">contacto@ansol.org</a> - <a href="https://ansol.org/contacto">http://ansol.org/contacto</a></p><ul><li>Rui Seabra <a href="mailto:rms@ansol.org">rms@ansol.org</a> - Presidente da Direção, Tel. 93 32-55-619</li><li>Marcos Marado <a href="mailto:marcos.marado@ansol.org">marcos.marado@ansol.org</a> - Vice Presidente da Direção, tel. 93 10-13-548</li></ul><p>Tugaleaks <a href="http://www.tugaleaks.com/contacto-colabora">http://www.tugaleaks.com/contacto-colabora</a></p><ul><li>Rui Cruz <a href="mailto:mail@ruicruz.pt">mail@ruicruz.pt</a>&nbsp;- Tel.&nbsp;96 82-71-502</li></ul><h3>SOBRE A ANSOL</h3><p>A Associação Nacional para o Software Livre é uma associação portuguesa sem fins lucrativos que tem como fim a divulgação, promoção, desenvolvimento, investigação e estudo da Informática Livre e das suas repercussões sociais, políticas, filosóficas, culturais, técnicas e científicas.</p><h3>SOBRE O TUGALEAKS</h3><p>O Tugaleaks é um repositório de informações online sobre a verdade da informação, leaks, protestos, movimentos cívicos e noticias em geral que os media não divulgam. Pauta-se pela liberdade, e assume que todos têm direito à informação, seja ela boa ou má, para fazerem as suas próprias escolhas pela vida fora.</p>
