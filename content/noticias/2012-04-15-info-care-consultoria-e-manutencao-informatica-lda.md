---
categories:
- consultoria
- suporte
metadata:
  email:
  - email_email: apoio@info-care.com.pt
  servicos:
  - servicos_tid: 7
  - servicos_tid: 2
  site:
  - site_url: http://info-care.com.pt
    site_title: http://info-care.com.pt
    site_attributes: a:0:{}
  node_id: 43
layout: servicos
title: INFO-CARE - Consultoria e Manutenção Informática, Lda.
created: 1334499974
date: 2012-04-15
aliases:
- "/node/43/"
- "/servicos/43/"
---
<h2>
	Consultoria</h2>
<p>Gest&atilde;o Log&iacute;stica da Informa&ccedil;&atilde;o. Integra&ccedil;&atilde;o de Sistemas de Informa&ccedil;&atilde;o. Sistemas de Partilha e Gest&atilde;o de Informa&ccedil;&atilde;o e Conhecimento.</p>
<h2>
	Suporte</h2>
<p>Apoio, suporte, diagn&oacute;stico, manuten&ccedil;&atilde;o e repara&ccedil;&atilde;o de computadores (hw - intel, ppc) (sw - gentoo, ubuntu) e sistema de comunica&ccedil;&atilde;o (lan, man, wan, wifi - cisco, draytek, eicon, e outros).</p>
