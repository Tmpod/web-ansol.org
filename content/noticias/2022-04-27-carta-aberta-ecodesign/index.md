---
categories:
- carta aberta
layout: article
title: Carta aberta - O Direito Universal para instalar qualquer software em qualquer dispositivo
date: 2022-04-27
---

A União Europeia está prestes a redefinir os critérios de ecodesign para
produtos em várias propostas legislativas, incluindo a **Iniciativa em matéria de
produtos sustentáveis**, a **Iniciativa sobre a Electrónica Circular**, e o **Direito à
Reparação**. Estas propostas têm como objectivo aumentar o tempo de vida de
hardware e facilitar o uso circular de dispositivos electrónicos. As
legislações actuais vêm de 2009 e não incluem quaisquer critérios no que toca
ao desenho e licenciamento de software como um factor importante para a
sustentabilidade de produtos electrónicos. O software influencia directamente o
tempo durante o qual consumidores usam os seus dispositivos.

Hoje em dia, utilizadores que queiram usar os seus dispositivos durante mais
tempo, ou reutilizar o seu hardware de forma criativa, encontram várias
barreiras a nível de software: obsolescência, fim de suporte abrupto, e
bootloaders trancados. Na práctica, estas restrições artificiais na utilização
e reutilização de hardware são impostas por software. Nem consumidores nem
serviços profissionais por terceiros conseguem ultrapassá-las, muitas vezes só
por causa dos modelos de licenciamento de software proprietário. **As licenças de
Software Livre resolvem muito destes problemas** e, como tal, tornam-se
essenciais para o desenho ecológico e para a sustentabilidade dos dispositivos
electrónicos; **[esta é a mensagem principal de uma carta aberta assinada hoje
por 38 organizações e empresas europeias][carta]**.

Os signatários iniciais desta carta aberta incluem organizações da sociedade
civil dos sectores ambiental, económico e tecnológico. A carta aberta inclui
também o apoio de várias empresas, mostrando que uma sociedade digital
sustentável e o crescimento económico não são objectivos contraditórios.

O texto completo da **[carta aberta está disponível no site da Free Software
Foundation Europe][carta]**.

Segue-se a lista dos signatários iniciais, por ordem alfabética:

* /e/ Foundation
* Associação Nacional para o Software Livre (ANSOL)
* European Open Source Business Association (APELL)
* Back Market
* Barcelona Free Software Group
* Citizen D
* Deutscher Naturschutzring
* Digitalcourage
* Digitale Gesellschaft CH
* Document Foundation
* Environmental Coalition on Standards
* Epicenter.works
* European Digital Rights (EDRi)
* Elektronisk Forpost Norge
* European Right to Repair Campaign (repair.eu)
* Fairphone
* Forum InformatikerInnen für Frieden und gesellschaftliche Verantwortung e.V. (FifF)
* Free Software Foundation Europe (FSFE)
* Germanwatch
* Greek Open Technologies Alliance (GFOSS)
* Heinlein Support
* iFixit
* KDE
* Mailbox.org
* Mouvement Ecologique
* Naturschutzbund Deutschland (NABU)
* Netzwerk Reparatur Initiativen
* Nextcloud
* Nitrokey
* Norwegian Unix User Group
* Oekozenter Pafendall
* Open Kowledge Foundation DE
* OPNTEC
* Open Source Business Alliance (OSBA)
* Runder Tisch Reparatur
* Wikimedia DE

[carta]: https://fsfe.org/activities/upcyclingandroid/openletter.pt.html
