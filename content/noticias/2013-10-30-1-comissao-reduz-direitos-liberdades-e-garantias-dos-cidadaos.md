---
categories:
- press release
- imprensa
metadata:
  tags:
  - tags_tid: 9
  - tags_tid: 19
  node_id: 211
layout: article
title: 1ª Comissão Reduz Direitos, Liberdades e Garantias dos cidadãos
created: 1383155892
date: 2013-10-30
aliases:
- "/article/211/"
- "/node/211/"
- "/pr-20131030/"
---
<p>Lisboa, 30 de Outubro de 2013 - A Associação Nacional para o Software Livre (ANSOL) lamenta com pesar que a 1ª Comissão Permanente dos Direitos Liberdades e Garantias tenha optado por, em sede de especialidade, favorecer o lobby da indústria de conteúdos em prejuízo dos direitos dos cidadãos pela parte da maioria.<br><br>Apesar da aprovação na generalidade dos projetos, <a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=37676">406</a> e <a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=37773">423</a> /XII/2a, que retomariam a justiça e devolveriam aos cidadãos os direitos que lhes foram roubados pela lei 50/2004 ao dar proteções indevidas ao DRM à margem da directiva <a href="http://en.wikipedia.org/wiki/European_Union_Copyright_Directive">2001/29/CE</a>, a maioria governativa chumbou o projeto conjunto na especialidade, com aprovação tácita do partido socialista via abstenção.</p><p>De acordo com o deputado Miguel Tiago do PCP, o "<em>PSD e CDS voltaram atrás no inicial apoio manifestado a estas iniciativas, por não terem receptividade do Governo, segundo os próprios</em>". Efetivamente a opinião frequente do Secretário de Estado é que as alterações ao Direito de Autor deveriam ser feitas num conjunto de discussão mais alargado de revisão geral da lei.</p><p>Acrescenta ainda que "<em>ao mesmo tempo, PS, PSD e CDS aprovaram o diploma que alarga o prazo de protecção de direitos conexos sobre obras fixadas em fonogramas, uma vez mais tomando o lado das empresas que exploram a obra, retardando a entrada das obras no domínio público.</em>" Aprovar a proposta de lei do Governo <a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=37885">169</a>/XII/2a favoreceu a indústria face os direitos dos cidadãos, estendendo dois prazos para 50 e 70 anos após data de publicação.<br><br>"<em>Primeiro prémio mais joker para o lobby da indústria</em>", diz Rui Seabra, presidente da Direção da ANSOL, "<em>quem perde são os cidadãos e vê-se bem quem tem a preferência da maioria: uma indústria obsoleta que rouba o domínio público e priva os cidadãos de direitos humanos. O PS necessita encontrar identidade própria, escolherá os cidadãos como apregoa Seguro? Perdeu uma oportunidade de o provar</em>.", conclui.<br><br>No Facebook, a co-coordenadora do BE e deputada Catarina Martins também lamenta a votação dizendo que " <em>Em vez de mais liberdade e sensatez, PSD e CDS escolhem manter o absurdo e mais uma forma de privatização do que é de todos; o acesso à arte, à cultura, ao conhecimento</em>."</p><p><a href="https://ansol.org/pr-20130610">Em Junho, a ANSOL tinha feito um forte apelo a todos os deputados da Assembleia da República</a> que aprovassem estes projetos, mas a votação na generalidade de mais de duas centenas de deputados foi revogada por 3 "pessoas".</p><ol><li><a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=37676">Projeto de Lei 406/XII/2ª</a></li><li><a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=37773">Projeto de Lei 423/XII/2ª</a></li><li><a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=37885">Proposta de Lei 169/XII/2ª</a></li></ol>
